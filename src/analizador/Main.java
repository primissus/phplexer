package analizador;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.System;
import java.util.Map;
import java.util.HashMap;
public class Main {
  private static String FILENAME = "file.php";
  public static void main(String[] args) {
    String texto = leerArchivo();
    if (analizar(texto)) {
      // System.out.print("El archivo está correcto");
    } else {
      // System.out.print("El archivo tiene errores");
    }
  }

  private static String leerArchivo() {
    String texto = "";
    try(BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
      String linea;
      while ((linea = br.readLine()) != null) {
        texto += linea + "\n";
      }
    } catch(IOException error) {
      error.printStackTrace();
      return null;
    }
    return texto;
  }

  private static boolean analizar(String texto) {
    Reglas reglas = new Reglas();
    String[] partes = texto.split(reglas.separador);
    String sintaxis =  "";
    for (String parte : partes) {
      String token = reglas.tokens.get(parte);
      if (token != null) {
        sintaxis += token + " ";
      }
    }
    System.out.println(sintaxis);
    return true;
  }
}