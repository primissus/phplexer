package analizador;

import java.util.HashMap;
import java.util.Map;

class Reglas {
  public Map<String, String> tokens;
  String separador = "(\\s|\n)+";

  public Reglas() {
    tokens = new RegExHashMap();
    tokens.put("\\<\\?", "inicio");
    tokens.put("\\?\\>", "fin");
    tokens.put(";", "delimitador");
    tokens.put("\\-?\\d+(\\.\\d+)?", "numero");
    tokens.put("\\$[a-zA-Z][a-zA-Z0-9_]*", "variable");
    tokens.put("\\(", "parIz");
    tokens.put("\\)", "parDer");
    tokens.put("\\{", "llavIz");
    tokens.put("\\}", "llavDer");
    tokens.put("=", "igual");
    tokens.put("==", "igualdad");
  }
}